you know `${var?error message here}` in sh?  it produces an error and prints the message if `$var` is undefined.  otherwise it expands to `$var`

introducing: `${var+${error message here}}`

if `$var` is defined, the spaces in the error message cause a syntax error, which prints the message.  otherwise it expands to nothing

(and similarly for all other outer expansions)

it reliably[^reliable] works[^works] perfectly[^dashzsh] in every[^shells] posix sh[^ext]

https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_06_02

> where expression consists of all characters until the matching '}'.

> In each case that a value of word is needed (based on the state of parameter, as described below),
> word shall be subjected to tilde expansion, **parameter expansion**, command substitution, and arithmetic expansion.
> If word is not needed, it **shall not be expanded.**

---

next:
[recursive expansions](https://gist.github.com/izabera/8c541886c3992d328255944bc3de62c7)
previous:
[reading a file line by line in your shell](https://gist.github.com/izabera/447c162219f25041a5aa4abb3dc255bd)

[^reliable]: shells are required to show a diagnostic message on syntax errors and expansion errors, but not necessarily what caused it

[^works]: in bash dash zsh & mksh

[^dashzsh]: dash and zsh only print "bad substitution"

[^shells]: ksh and yash don't like this trick at all and (incorrectly) just error out unconditionally

[^ext]: nothing in the standard prevents a shell from supporting spaces in expansions as an extension, making this not an error